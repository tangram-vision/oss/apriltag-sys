# apriltag-sys

This crate provides Rust bindings for AprilTag C library.

## Usage

Install AprilTag library from the Tangram Vision fork of AprilRobotics' AprilTag
repository [repository](https://gitlab.com/tangram-vision-oss/apriltag).

Import `apriltag-sys` dependency in your `Cargo.toml`

```toml
[dependencies]
apriltag-sys = "0.1"
```

### Specifying how to compile and link the apriltag C library.

For this modification in the Tangram Vision SDK, we only allow static linking.

The location of the apriltag source is specified by the `APRILTAG_SRC`
environment variable. If this is not set, Cargo will look for the source under
the folder `apriltag` on the same level as `apriltag-sys`.

#### Building under Windows

This library modification has not been tested on Windows machines.

## License

BSD-2-Clause. Please see [license file](LICENSE).
